#!/usr/bin/env bash
module load lightspeed
export CUDA_VISIBLE_DEVICES=5
python tensor.py
python resource_list.py
python molecule.py
python basis.py
python pairlist.py
python sad.py
python intbox.py
python diis.py
python davidson.py
