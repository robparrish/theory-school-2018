import lightspeed as ls
import numpy as np

# => Load Molecule <= #

# Load Molecule from XYZ string representation
mol = ls.Molecule.from_xyz_str("""
    3
    0 1
    O          0.000000000000     0.000000000000    -0.068516219310 
    H          0.000000000000    -0.790689573744     0.543701060724 
    H          0.000000000000     0.790689573744     0.543701060724
    """,
    name='h2o',
    )
print mol

# => Load Basis <= #

basis = ls.Basis.from_gbs_file(mol, 'cc-pvdz')
print basis

# => Build PairList <= #

pairlist = ls.PairList.build_schwarz(
    basis,    # The first basis set
    basis,    # The second basis set
    True,     # The PairList is symmetric in basis1 and basis2
    1.0E-14,  # The Schwarz sieve cutoff
    )
print pairlist

