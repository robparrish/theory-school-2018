import lightspeed as ls
import numpy as np
import vv

def nve_dynamics(
    lot,
    S,
    index,
    M,
    P0,
    dt=20.0,
    tmax=200.0,
    xyzfilename='mol.xyz',
    xyzframerate=4,
    ):

    V0 = lot.compute_energy(S, index)
    F0 = lot.compute_gradient(S, index)
    F0.scale(-1.0)
    X0 = lot.xyz

    prop = vv.VV(dt,M,X0,P0,F0,V0)

    stats = {
        't' : [],
        'T' : [],
        'V' : [],
        'E' : [],
    }

    print 'NVE: %5s %14s %24s %24s %24s' % (
        "I",
        "t", 
        "T",
        "V",
        "E",
        )

    while prop.t < tmax:

        print 'NVE: %5d %14.6f %24.16E %24.16E %24.16E' % (
            prop.I,
            prop.t,
            prop.T,
            prop.V,
            prop.E,
            )
        stats['t'].append(prop.t)
        stats['T'].append(prop.T)
        stats['V'].append(prop.V)
        stats['E'].append(prop.E)

        if prop.I % xyzframerate == 0:
            lot.molecule.save_xyz_file(xyzfilename,append=(False if prop.I == 0 else True))

        lot = lot.update_xyz(prop.Xnew)
        V = lot.compute_energy(S, index)
        F = lot.compute_gradient(S, index)
        F.scale(-1.0)

        prop.step(F,V)

    stats = { key : np.array(val) for key, val in stats.iteritems() }

    return stats

def mass_vector(molecule):

    # in Da or u (from the internets)
    mass_table = {
        'H'  :  1.0078,
        'C'  : 12.0,
        'N'  : 14.0031,
        'O'  : 15.99491461956,
        'F'  : 18.9984,
        'P'  : 22.9898, 
        'S'  : 31.9721,
    }

    # 1 u in terms of au (m_e)
    au_per_u = 1822.88848637

    M = ls.Tensor.zeros((molecule.natom,3))
    for A, atom in enumerate(molecule.atoms):
        M[A,:] = mass_table[atom.symbol] * au_per_u
    return M

def momentum_vector(M, T):

    return ls.Tensor.zeros_like(M) # TODO

