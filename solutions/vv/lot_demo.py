import lightspeed as ls # Lightspeed does not do electronic structure for you...
import psiw # but psidewinder (psiw) does!

if __name__ == '__main__':

    # Some resources to use for a dynamics problem
    resources = ls.ResourceList.build()

    # An initial set of molecular coordinates
    molecule = ls.Molecule.from_xyz_file('geom/x.xyz')

    # A Geometry object specifying all concerns for an EST problem
    geometry = psiw.Geometry.build(
        resources=resources,
        molecule=molecule,
        basisname='6-31gss',
        basisspherical=False,
        )

    # FON-RHF/6-31G** with a (2e,2o) fractional space
    ref = psiw.RHF.from_options(
        geometry=geometry,
        g_convergence=1.0E-7,
        fomo=True,
        fomo_method='gaussian',
        fomo_temp=0.2,
        fomo_nocc=39,
        fomo_nact=2,
        )
    ref.compute_energy()

    # FOMO-CASCI(2e,2o)/6-31G**
    casci = psiw.CASCI.from_options(
        reference=ref,
        nocc=39,
        nact=2,
        nalpha=1,
        nbeta=1,
        S_inds=[0],
        S_nstates=[2],
        ) 
    casci.compute_energy()

    # A "level-of-theory" wrapper for FOMO-CASCI
    lot = psiw.CASCI_LOT.from_options(
        casci=casci,
        print_level=1,
        rhf_guess=True,
        rhf_mom=True,
        )
    # LOT has a number of standard functions. These include: 
    # Computing the energy of a given state, here for S=0, index=0
    print lot.compute_energy(S=0, index=1)
    # Computing the gradient of a given state, here for S=0, index=0
    print lot.compute_gradient(S=0, index=1)
    # Updating the coordinates, including guess/MOM propagation
    lot2 = lot.update_xyz(molecule.xyz)
    

