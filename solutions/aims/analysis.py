import numpy as np
import aimsprop as ai
import glob, re, sys, os, copy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

if __name__ == '__main__':

    time_scale = 1.0 / 41.3413745758  # au to fs

    # parse each initial condition
    ICs = [0,1,2,3,4,5,6]
    trajs = [ai.parse_aimspy('trajs/%04d' % IC, scheme='mulliken') for IC in ICs]

    # Merge the trajectories into one super-big Trajectory with uniform weights
    traj = ai.Trajectory.merge(trajs, [1.0 / len(trajs)] * len(trajs), labels=ICs)
    ts = traj.ts

    # Compute properties at ~1/2 fs intervals, removing nonsense due to adaptive timesteps
    ts = np.arange(np.min(traj.ts), np.max(traj.ts), 20.0)
    traj = traj.interpolate_nearest(ts)
    
    # Plot Population Decay
    pop = ai.plot_population('pop.pdf', traj, trajs)

    # Compute symmetric Torsions
    traj = ai.compute_torsion(traj, 'TOR1', 2, 0, 1, 5)
    traj = ai.compute_torsion(traj, 'TOR2', 3, 0, 1, 4)
    traj = ai.unwrap_property(traj, 'TOR1', 360.0)
    traj = ai.unwrap_property(traj, 'TOR2', 360.0)

    ai.plot_scalar(
        'TOR.pdf', 
        traj, 
        'TOR1', 
        ylabel='Torsion Angle', 
        time_units='fs', 
        state_colors = [[1.0,0.0,0.0,0.5], [0.0,0.0,1.0,0.5]], 
        plot_average=False,
        )
    ai.plot_scalar(
        'TOR.pdf', 
        traj, 
        'TOR2', 
        ylabel='Torsion Angle', 
        time_units='fs', 
        state_colors = [[1.0,0.0,0.0,0.5], [0.0,0.0,1.0,0.5]], 
        plot_average=False,
        )
    plt.savefig('TOR.pdf', bbox_inches='tight')

