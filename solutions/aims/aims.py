import numpy as np
import lightspeed as ls
import psiw
import aimspy as ps
import sys

def run_aims(
    xfilename,
    pfilename,
    ):

    resources = ls.ResourceList.build()

    molecule = ls.Molecule.from_xyz_file(xfilename)
    
    geom = psiw.Geometry.build(
        resources=resources,
        molecule=molecule,
        basisname='6-31gss',
        )
    
    ref = psiw.RHF.from_options(
        print_level=0,
        geometry=geom,
        g_convergence=1.0E-6,
        fomo=True,
        fomo_method='gaussian',
        fomo_temp=0.25,
        fomo_nocc=7,
        fomo_nact=2,
        )
    ref.compute_energy()
    
    casci = psiw.CASCI.from_options(
        print_level=0,
        reference=ref,
        nocc=7,
        nact=2,
        nalpha=1,
        nbeta=1,
        S_inds=[0],
        S_nstates=[2],
        )
    casci.compute_energy()
    
    lot = psiw.CASCI_LOT.from_options(
        casci=casci,
        print_level=1,
        rhf_guess=True,
        rhf_mom=True,
        )

    # construct wavefunction from xyz files
    wfn = ps.Wavefunction.wfn_from_xyzs(
        coordinates=[xfilename],
        momenta=[pfilename],
        states=[1],
        )

    # Instantiate est, hamiltonian, propagator and spawning objects
    est = ps.EST.from_options(
        lot=lot, 
        v_offset=78.5,
        )
    hamiltonian = ps.Hamiltonian.from_options(
        est=est,
        centroid=False,
        )
    qm_propagator = ps.Magnus_expansion(hamiltonian)
    classical_prop = ps.Velocity_verlet(est)
    spawn = ps.STDSpawn.from_options(
        est=est,
        classical_prop=classical_prop,
        thresh_dv_coupling=0.04,
        nstates=2, 
        thresh_coupling_entry=0.01,
        thresh_tdc_spawn=0.01,
        thresh_overlap=0.6, 
        thresh_pop=0.05,
        )
    aims = ps.AIMS.from_options(
        dt=20.0,
        dt_coupling=5.0,
        t_final=4000.0,
        est=est,
        hamiltonian=hamiltonian,
        qm_prop=qm_propagator,
        classical_prop=classical_prop,
        spawn=spawn,
        wfn=wfn,
        )
    aims.run()

if __name__ == '__main__': 

    xfilename = sys.argv[1]
    pfilename = sys.argv[2]
    run_aims(xfilename, pfilename)
