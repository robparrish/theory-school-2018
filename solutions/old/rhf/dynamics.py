import lightspeed as ls
import numpy as np
import rhf
import vv

def nve_dynamics(
    ref,
    M,
    P0,
    dt=20.0,
    tmax=200.0,
    xyzfilename='mol.xyz',
    xyzframerate=4,
    ):

    V0 = ref.compute_energy()
    F0 = ref.compute_gradient()
    F0.scale(-1.0)
    X0 = ref.molecule.xyz

    prop = vv.VV(dt,M,X0,P0,F0,V0)

    stats = {
        't' : [],
        'T' : [],
        'V' : [],
        'E' : [],
    }

    print 'NVE: %5s %14s %24s %24s %24s' % (
        "I",
        "t", 
        "T",
        "V",
        "E",
        )

    while prop.t < tmax:

        print 'NVE: %5d %14.6f %24.16E %24.16E %24.16E' % (
            prop.I,
            prop.t,
            prop.T,
            prop.V,
            prop.E,
            )
        stats['t'].append(prop.t)
        stats['T'].append(prop.T)
        stats['V'].append(prop.V)
        stats['E'].append(prop.E)

        if prop.I % xyzframerate == 0:
            ref.molecule.save_xyz_file(xyzfilename,append=(False if prop.I == 0 else True))

        ref = ref.update_xyz(prop.Xnew)
        V = ref.compute_energy()
        F = ref.compute_gradient()
        F.scale(-1.0)
        if not ref.converged:
            raise ValueError('RHF did not converge')

        prop.step(F,V)

    stats = { key : np.array(val) for key, val in stats.iteritems() }

    return stats

def mass_vector(molecule):

    # in Da or u (from the internets)
    mass_table = {
        'H'  :  1.0078,
        'C'  : 12.0,
        'N'  : 14.0031,
        'O'  : 15.99491461956,
        'F'  : 18.9984,
        'P'  : 22.9898, 
        'S'  : 31.9721,
    }

    # 1 u in terms of au (m_e)
    au_per_u = 1822.88848637

    M = ls.Tensor.zeros((molecule.natom,3))
    for A, atom in enumerate(molecule.atoms):
        M[A,:] = mass_table[atom.symbol] * au_per_u
    return M

def momentum_vector(M, T):

    return ls.Tensor.zeros_like(M) # TODO

def run_h2o():

    mol = ls.Molecule.from_xyz_str("""
        O 0.000000000000  0.000000000000 -0.129476890157
        H 0.000000000000 -1.494186750504  1.027446102928
        H 0.000000000000  1.494186750504  1.027446102928""", 
        name='h2o',
        scale=1.0)

    ref = rhf.RHF.build(
        ls.ResourceList.build(1024,1024),
        mol,
        basisname='cc-pvdz',
        minbasisname='cc-pvdz-minao',
        options={
            'g_convergence'  : 1.0E-6,
            'print_orbitals' : False,
        })

    M = mass_vector(mol)
    
    P0 = momentum_vector(M,0.0)

    stats = nve_dynamics(
        ref,
        M,
        P0,
        dt=20.0,
        tmax=5000.0,
        ) 

    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    
    plt.clf()
    plt.plot(stats['t'],stats['T']-stats['T'][0],label='T')
    plt.plot(stats['t'],stats['V']-stats['V'][0],label='V')
    plt.plot(stats['t'],stats['E']-stats['E'][0],label='E')
    plt.xlabel('t')
    plt.ylabel('E')
    plt.legend()
    plt.savefig('E.pdf')

if __name__ == '__main__':

    run_h2o() 
