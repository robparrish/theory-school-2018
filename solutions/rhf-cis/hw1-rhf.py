import lightspeed as ls
import rhf
import sys
import time
import os

molname = sys.argv[1]
basisname = sys.argv[2]

# Timestamp
start_time = time.time()
print '*** Started at: %s on %s ***' % (
           time.strftime("%a %b %d %H:%M:%S %Z %Y", time.localtime()),
           os.uname()[1],
           )

resources = ls.ResourceList.build()

molecule = ls.Molecule.from_xyz_file(molname)

ref = rhf.RHF.build(
    resources,
    molecule,
    basisname=basisname)
ref.compute_energy()

# Timestamp
stop_time = time.time()
print '*** Stopped at: %s on %s ***' % (
           time.strftime("%a %b %d %H:%M:%S %Z %Y", time.localtime()),
           os.uname()[1],
           )
print '*** Runtime: %.3f [s] ***\n' % (
        stop_time - start_time)
